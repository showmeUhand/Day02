package com.example.Day02.POJO;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet("/ser01")
public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        String uid = req.getParameter("id");
        String upassword = req.getParameter("password");

        //连接数据库
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/User","root","Zhang1221?");
            Statement st = con.createStatement();
            String sql="select * from User where id=? and password=?";
            PreparedStatement pst =con.prepareStatement(sql);
            pst.setString(1,uid);
            pst.setString(2,upassword);
            ResultSet rs=pst.executeQuery();
            //System.out.println("这是pst:"+pst);
           // System.out.println("这是rs"+rs);
            if(rs.next()){
                System.out.println("登陆成功");
                resp.sendRedirect("suf.jsp");
            }else{
                System.out.println("登陆失败");
                resp.sendRedirect("Login.jsp");
            }
            ResultSet rst =pst.executeQuery();
            con.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }

    }
}
